#CSS Smell#
Your CSS stinks! Let's clean it up. Now a Heroku app!

[ ![Codeship Status for csci6233csssmell/css-smell](https://codeship.com/projects/87a616f0-6969-0133-08d6-16d2a9563249/status?branch=master)](https://codeship.com/projects/114538)

##Usage##
- Requires node/npm installed
- Run `npm install` in the project directory
- launch with `node ./bin/www` or `npm run-script start`
- Access the tool by going to localhost:3000 in your web browser
- The root page allows for file upload or text input
- Once the file is uploaded or text is entered click submit and you should see the lint results
- If no errors are encountered you will be re-directed to the upload form
- Note: The behavior of uploading non-css files/text is undefined

##Development##
- Make sure you have node and npm installed
	- See https://nodejs.org/en/
- If you want to use the included gruntfile (easily) you must have grunt installed
	- See http://gruntjs.com/
	- Basically just do `npm install -g grunt-cli`

- Once you have cloned the repo make sure to run `npm install`
- If you have grunt-cli installed, then just cd to the project root, and type `grunt`
- If you don't have grunt-cli installed then cd to the project root, and type `npm run-script start`

##Adding a lint rule##
- Lint rules should be added to the linters directory
- You do not need to add a reference to the new rule, simply ensure you assign your lint function to module.exports
	- Check emptyrule.js for an example
- The sole parameter to the function is the item being tested, only import, rule, and comment blocks are passed to the linters
	- Your lint function must correctly handle the targeted block type
- Lint functions should be hyper-specific, don't generalize
	- To take a look at how the objects that compose the arrays are constructed you can throw test css at http://iamdustan.com/reworkcss_ast_explorer/

##Running Tests##

- Ensure you have the grunt cli installed (`npm install -g grunt-cli`)
- To run all of the tests use `grunt test`
- To run a specific test use `casperjs test tests/<test-file>`
	- This implies that casperjs is already installed, if it is not you can install it using `npm install -g casperjs`
