var express = require('express');
var router = express.Router();
var multer = require('multer')({dest: 'tmp/'});
var parser = require('css');
var fs = require('fs');
var linters = require('../linters');
/* GET home page. */

router.get('/', function(req, res) {
  res.render('index', { title: 'CSS Smell' });
});

router.post('/', multer.single('css_file'), function(req, res) {
	//prefer the file input over the textarea
	var content;
	if(req.file && req.file.path) {
		//read the file from the tmp folder
		content = fs.readFileSync(req.file.path, 'utf8');
	} else {
		//branch to deal with textbox input
		content = req.body.css_text;
	}
	var css = parser.parse(content, {
		silent: true
	});
	var output = linters(css.stylesheet.rules);
	res.render('index', {
		linting: output,
		errors: css.stylesheet.parsingErrors,
		noErrors: output.length < 1 && css.stylesheet.parsingErrors.length < 1
	});
});

module.exports = router;
