#Test Spec#

##Tools##
- Grabbed casperjs as this allows us to do simple unit testing as well as browser based testing
- The tool is configured to run the tests through grunt, so you will need that to test (`npm install -g grunt-cli`)
	- Once downloaded make sure you run `npm install-dev` to get the dev dependencies, including casperjs

##Web Application##
- Test upload of file
- Test input of text
- Test result page
	- Look for known correct output of test file/text

##Linters##
- `require` the linters, then pass known good and known bad input
- use `assert` to test the return value and ensure it responds correctly

##UX##
- go through the flow of use
- what improvements could be made to improve usability?
