casper.test.begin('Basic usability - page loads, and displays the form', 5, function(test) {
  casper.start('http://localhost:3000', function() {
    test.assertTitle('CSS Smell', 'Title is expected value of "CSS Smell"');
    test.assertExists('form#css_form', 'The upload form exists');
    test.assertExists('input[type="file"]#css_file', 'The file upload input exists');
    test.assertExists('textarea[name="css_text"]', 'The text input field exists');
    test.assertExists('button#submit', 'The submit button exists');
  });
  casper.run(function() {
    test.done();
  });
});
