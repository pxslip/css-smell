var adjoining_classes = require('../linters/adjoining_classes.js');
var noResultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white'
  }]
};
var resultItem = {
  type: 'rule',
  selectors: [
    '.class1.class2'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white'
  }]
};

/**
 * casper.test.begin is a function that takes parameters in the order message, numTests, suite
 * message: the message to show for this test suite
 * numTests: the number of tests that are run in this test suite
 * suite: the function that runs the tests, takes one parameter test that contains the testing methods
 */

casper.test.begin('adjoining_classes Rule Linter works', 3, function(test) {
  var noResult = adjoining_classes(noResultItem);
  test.assertEquals(noResult.result, 'passed', 'adjoining_classes gives a result of "passed" when the selector contains only non-adjoined classes/selectors');
  var result = adjoining_classes(resultItem);
  test.assertEquals(result.result, 'failed', 'adjoining_classes gives a result of "failed" when the selector contains two classes adjoined');
  test.assertEquals(result.message, 'You used adjoining classes', 'adjoining_classes responds with the correct message');
  test.done();
});
