var excl_important = require('../linters/excl_important.js');
var noResultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white'
  }]
};
var resultItem = {
  type: 'rule',
  selectors: [
    '.class1.class2'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white !important'
  }]
};

/**
 * casper.test.begin is a function that takes parameters in the order message, numTests, suite
 * message: the message to show for this test suite
 * numTests: the number of tests that are run in this test suite
 * suite: the function that runs the tests, takes one parameter test that contains the testing methods
 */


casper.test.begin('!important Rule Linter works', 3, function(test) {
  var noResult = excl_important(noResultItem);
  test.assertEquals(noResult.result, 'passed', 'excl_important gives a result of "passed" when the declaration contains no !important');
  var result = excl_important(resultItem);
  test.assertEquals(result.result, 'failed', 'excl_important gives a result of "failed" when the declaration contains !important');
  test.assertEquals(result.message, 'You used !important', 'excl_important responds with the correct message');
  test.done();

});
