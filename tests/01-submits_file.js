//if this test fails you may need to tinker with the value of this as it is necessary to find the css file we want to upload
var projPath = require('system').env.PWD;
casper.test.begin('The tool is able to submit a file to the server', 2, function(test) {
  casper.start('http://localhost:3000', function() {
    this.fill('form#css_form', {
      css_file: projPath+'/tests/test-files/passes.css'
    }, true);
  });
  casper.then(function() {
    test.assertExists('.alert.alert-success', 'The success message exists');
    test.assertSelectorHasText('.alert.alert-success', 'Your code smells beautiful! Have a haiku:', 'The success message partially matches the expectation');
  });
  casper.run(function() {
    test.done();
  });
});
