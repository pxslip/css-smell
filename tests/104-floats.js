
var floats = require('../linters/floats.js');
var noResultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white'
  }],
  position: {
    start: {
      line: 0
    }
  }
};
var resultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: [{
    type: 'declaration',
    property: 'float',
    value: 'left'
  }],
  position: {
    start: {
      line: 0
    }
  }
};

/**
 * casper.test.begin is a function that takes parameters in the order message, numTests, suite
 * message: the message to show for this test suite
 * numTests: the number of tests that are run in this test suite
 * suite: the function that runs the tests, takes one parameter test that contains the testing methods
 */


casper.test.begin('Floats Rule Linter works', 3, function(test) {
  var noResult = floats(noResultItem);
  test.assertEquals(noResult.result, 'passed', 'floats gives a result of "passed" when the declaration contains less than 10 float property');
  var result;
  for(var i = 0; i < 10; i++) {
    resultItem.position.start.line++;
    result = floats(resultItem);
  }
  test.assertEquals(result.result, 'failed', 'floats gives a result of "failed" when the declaration contains 10 or more of float property');
  test.assertEquals(result.message, 'You used the property float 10 times at this point', 'floats responds with the correct message');
  test.done();

});
