var excl_import = require('../linters/import.js');
var noResultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white'
  }]
};
var resultItem = {
  type: 'import',
  'import': 'style.css'
};

/**
 * casper.test.begin is a function that takes parameters in the order message, numTests, suite
 * message: the message to show for this test suite
 * numTests: the number of tests that are run in this test suite
 * suite: the function that runs the tests, takes one parameter test that contains the testing methods
 */


casper.test.begin('@import Rule Linter works', 3, function(test) {
  var noResult = excl_import(noResultItem);
  test.assertEquals(noResult.result, 'passed', 'excl_import gives a result of "passed" when the declaration contains no @import');
  var result = excl_import(resultItem);
  test.assertEquals(result.result, 'failed', 'excl_import gives a result of "failed" when the declaration contains @import');
  test.assertEquals(result.message, 'Do not use @import: ' + resultItem.import  +'. Use the link element, <link> instead.', 'excl_import responds with the correct message');
  test.done();

});
