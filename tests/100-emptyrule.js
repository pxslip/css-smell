var emptyrule = require('../linters/emptyrule.js');
var noResultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: [{
    type: 'declaration',
    property: 'color',
    value: 'white'
  }]
};
var resultItem = {
  type: 'rule',
  selectors: [
    '.class1'
  ],
  declarations: []
};


casper.test.begin('Empty Rule Linter works', 3, function(test) {
  var noResult = emptyrule(noResultItem);
  test.assertEquals(noResult.result, 'passed', 'Empty Rule gives a result of "passed" when the rule contains declarations');
  var result = emptyrule(resultItem);
  test.assertEquals(result.result, 'failed', 'Empty Rule gives a result of "failed" when the rule contains no declarations');
  test.assertEquals(result.message, 'You should remove empty rules before deploying your CSS', 'Empty Rule responds with the correct message');
  test.done();
});
