//this is slightly naughty, but we cannot handle this except by making this scoped outside of the function
var count = 0;
var lastLine = 0;
module.exports = function(item) {
  var hasFloat = false;
  if (item.type === 'rule') {
    if(item.position.start.line < lastLine) {
      //this is a bit of a hack, as the order is not necessarily guaranteed
      count = 0;
    }
    lastLine = item.position.start.line;
    var selectors = (item.selectors) ? item.selectors : [];
    var declarations = (item.declarations) ? item.declarations : [];
    // iterate through all the rules
    for (var i = 0; i < declarations.length; i++) {
      //check the value
      if (declarations[i].property === 'float') {
        //count the number of word float that appears
        count += 1;
        hasFloat = true;
      }
    }

    if (count >= 10 && hasFloat) {
      return {
        result: 'failed',
        message: 'You used the property float ' + count + ' times at this point'
      };
    }
  }
  return {
    result: 'passed'
  };
};
