/*
	adjoining_classes.js: will check the class name and make sure that no adjoining classes of the form .X.Y are present

    programmer: Basil Huffman (bahuffma@gwmail.gwu.edu)

	input parameters:
	- selectors: the name of the class
	- declarations: the rules

	output:
	- if passed: return a simple "passed" message
	- if failed: return a message indicating as such
*/

module.exports = function(item) {

  if (item.type === 'rule') {
    var selectors = (item.selectors) ? item.selectors : [];
    var declarations = (item.declarations) ? item.declarations : [];
    //iterate through the selectors/class names
    for (var i = 0; i < selectors.length; i++) {
      //convert selector name to a string
      var sel = '' + selectors[i];

      //use a regular expression to create an array of classnames
      //  search for '.' to exist multiple times which indicates adjoining classes
      var matches = sel.match(/\./g);

      //null indicates no matches and then check for more than one instance of .
      if (matches !== null && matches.length > 1) {
        return {
          result: "failed",
          message: "You used adjoining classes"
        };
      }
    }
  }
  return {
    result: "passed"
  };
};
