/*
	import.js: checks to see if an import is used

	developer: Will Kruse (wskruse@gmail.com)

	input parameters:
	- selectors: the name of the class
	- declarations: the rules, declaration[i].value contains the rule itself
  - type: the type of rule (import or rule)

	output:
	- if passed: return a simple "passed" message
	- if failed: return a message indicating as such, as well as a message listing that it has failed, and the rule number plus 1 which will aid in printing the line number
*/

module.exports = function(item) {
  if(item.type === 'import') {
    return {
      result: 'failed',
      message: 'Do not use @import: ' + item.import  +'. Use the link element, <link> instead.'
    };
  }
  return {
    result: 'passed'
  };
};
