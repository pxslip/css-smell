/*

The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


//do an automated loading of all of the files in this folder
//each file must export a single function with the signature function(rule, content) where rule and content will be an array of elements
//each function must return an object
var fs = require('fs');
var path = require('path');
var linters = [];
fs.readdirSync(__dirname).forEach(function(file) {
  if (file !== 'index.js') {
    var linter = require(path.join(__dirname, file));
    linters.push(linter);
  }
});
//let the magic happen! this is the master function that knows about the entire CSS structure
var composer = function(rules) {
  var output = [];
  rules.forEach(function(item, index, array) {
    //set up the output object for later use
    var rule = {
      start: item.position.start.line,
      end: item.position.end.line,
      messages: []
    };
    //we may need to do some top level linting stuff, like checking for use of floats here
    switch (item.type) {
      case 'media':
        //get the child rules and recurse into them
        output = output.concat(composer(item.rules));
        break;
      default:
        linters.forEach(function(linter, index, array) {
          var response = linter(item);
          if (response.result === "failed") {
            rule.messages.push(response.message);
          }
        });
        if (rule.messages.length > 0) {
          output.push(rule);
        }
        break;
    }
  });
  return output;
};
module.exports = composer;
