/**
 * Author: Will Kruse
 * emptyrule.js - checks if the declarations of a CSS rule is empty
 * input:
 * 	selectors: the CSS rule as an array of selectors e.g. .class1,.class2 becomse [.class1, .class2]
 * 	declarations: the property:value pairings of this rule
 *
 * output:
 * 	an object containing either a result of passed, or a result of failure and a message
 */
module.exports = function(item) {
  if (item.type === 'rule') {
    var selectors = (item.selectors) ? item.selectors : [];
    var declarations = (item.declarations) ? item.declarations : [];
    if (declarations && declarations.length === 0) {
      return {
        result: "failed",
        message: "You should remove empty rules before deploying your CSS"
      };
    }
  }
  return {
    result: "passed"
  };
};
