/*
	excl_important.js: checks to see if !important is present in the rule

	programmer: Basil Huffman (bahuffma@gwmail.gwu.edu)

	input parameters:
	- selectors: the name of the class
	- declarations: the rules, declaration[i].value contains the rule itself

	output:
	- if passed: return a simple "passed" message
	- if failed: return a message indicating as such, as well as a message listing that it has failed, and the rule number plus 1 which will aid in printing the line number
*/

module.exports = function(item) {
  if (item.type === 'rule') {
    var selectors = (item.selectors) ? item.selectors : [];
    var declarations = (item.declarations) ? item.declarations : [];
    // iterate through all the rules
    for (var i = 0; i < declarations.length; i++) {
      //get at index
      var dec = declarations[i];

      //get the value as a string
      var str = '' + dec.value;

      //check to see if !important is present
      var pos1 = str.search("!important");
      var pos2 = str.search("! important");

      // is pos is -1, that means that !important is not present
      if (pos1 != -1 || pos2 != -1) {
        return {
          result: "failed",
          message: "You used !important"
        };
      }
    }
  }
  return {
    result: "passed"
  };
};
